package com.tracks.likes;

import com.tracks.likes.model.Person;
import com.tracks.likes.model.Track;
import com.tracks.likes.service.PersonService;
import com.tracks.likes.service.TrackService;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.InputStream;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@SpringBootApplication
public class LikesApplication implements CommandLineRunner {

    @Autowired
    private PersonService personService;

    @Autowired
    private TrackService trackService;

    public static void main(String[] args) {
        SpringApplication.run(LikesApplication.class, args);
    }

    @Bean
    public Vertx getVertx() {
        return Vertx.vertx();
    }


    @Override
    public void run(String... args) throws Exception {
        // Entry point of program
        PrintStream printStream = System.out;
        InputStream inputStream = System.in;

        Person person = new Person();
        person.setNickname("Person1");
        person.setAge(23);
        //work asynchronously
        Future<Person> personFuture = personService.registerPerson(person);

        Track track = new Track();
        track.setTitle("Some track");
        track.setArtist("SomeArtist");
        //work asynchronously
        Future<Track> trackFuture = trackService.saveAndValidate(track);

        //wait complete of savings person and track an then add like
        CompositeFuture.all(personFuture, trackFuture)
                .setHandler(asyncResult-> {
                    List<Object> list = asyncResult.result().list();
                    Person savedPerson = (Person) list.get(0);
                    Track savedTrack = (Track) list.get(1);

                    personService.addLike(savedPerson.getId(), savedTrack.getId())
                            .setHandler(voidResult -> {
                                //handler will invoke when addLike will return result
                                if (voidResult.failed()) {
                                    printStream.print(voidResult.cause().getMessage());
                                }
                                printStream.print("like added successfully!");
                            });
                });

    }
}
