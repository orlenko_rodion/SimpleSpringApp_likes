package com.tracks.likes.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "person")
public class Person {

    @Id
    private String id;
    @Indexed(unique = true)
    private String nickname;
    private int age;
    private List<Track> likedTracks = new ArrayList<>();

    //todo add other fields
}
