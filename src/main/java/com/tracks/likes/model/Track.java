package com.tracks.likes.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Timestamp;

@Data
@Document(collection = "track")
public class Track {

    @Id
    private String id;
    private String title;
    private String album;
    private String artist;
    private Timestamp duration;
    private int likes;

    public int incrLikes(int count) {
        return likes += count;

    }
}
