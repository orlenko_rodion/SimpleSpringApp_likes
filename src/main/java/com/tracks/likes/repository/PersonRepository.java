package com.tracks.likes.repository;

import com.tracks.likes.model.Person;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonRepository extends MongoRepository<Person, String> {
    Person findByNickname(String nickname);
    Person deleteByNickname(String nickname);
}
