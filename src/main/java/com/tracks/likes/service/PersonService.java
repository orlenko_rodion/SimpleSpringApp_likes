package com.tracks.likes.service;

import com.tracks.likes.model.Person;
import com.tracks.likes.model.Track;
import com.tracks.likes.repository.PersonRepository;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

@Service
public class PersonService {

    private PersonRepository personRepository;

    private TrackService trackService;

    private Vertx vertx;


    public PersonService(PersonRepository personRepository,
                         TrackService trackService,
                         Vertx vertx) {
        this.personRepository = personRepository;
        this.trackService = trackService;
        this.vertx = vertx;
    }

    /**
     *
     *
     * @param person
     * @return
     */
    public Future<Person> registerPerson(Person person) {
        Future<Person> resultFuture = Future.future();

        vertx.executeBlocking(blockingFuture -> {
            validatePerson(person);
            // complete
            blockingFuture.complete(personRepository.save(person));
        }, personAsyncResultHandler(resultFuture));

        return resultFuture;
    }

    public Future<Person> getPersonByNickName(String nick) {
        Future<Person> resultFuture = Future.future();

        vertx.executeBlocking(blockingFuture -> blockingFuture.complete(personRepository
                .findByNickname(nick)), personAsyncResultHandler(resultFuture));
        return resultFuture;
    }

    public Person getPersonById(String personId) {
        Optional<Person> personOptional = personRepository.findById(personId);
        if (!personOptional.isPresent()) {
            throw new NoSuchElementException("Person with such id does not exist");
        }
        return personOptional.get();
    }

    public Future<Void> addLike(String personId, String trackId) {
        Future<Void> resultFuture = Future.future();

        vertx.executeBlocking(blockingFuture -> {
            Person person = getPersonById(personId);
            trackService.getTrackById(trackId)
                    .setHandler(trackAsyncResult -> {
                        if (trackAsyncResult.failed()) {
                            blockingFuture.fail(trackAsyncResult.cause());
                            return;
                        }
                        Track track = trackAsyncResult.result();
                        Objects.requireNonNull(person, "Person is not exist");
                        Objects.requireNonNull(track, "Track is not exist");
                        track.incrLikes(1);
                        person.getLikedTracks().add(track);
                        trackService.saveAndValidate(track);

                        personRepository.save(person);
                        blockingFuture.complete();
                    });
        }, voidAsyncResultHandler(resultFuture));

        return resultFuture;
    }

    private void validatePerson(Person person) {
        Objects.requireNonNull(person, "Person should not be null");
        if (person.getAge() <= 0) {
            throw new IllegalArgumentException("Person's age is incorrect");
        }
        Objects.requireNonNull(person.getNickname(), "Person's nickname should not be null");
        if (Objects.nonNull(personRepository.findByNickname(person.getNickname()))) {
            throw new IllegalArgumentException("Person with such nickname already exists!");
        }
    }

    private Handler<AsyncResult<Person>> personAsyncResultHandler(Future<Person> resultFuture) {
        return asyncResult -> {
            if (asyncResult.failed()) {
                resultFuture.fail(asyncResult.cause());
                return;
            }
            resultFuture.complete(asyncResult.result());
        };
    }

    private Handler<AsyncResult<Void>> voidAsyncResultHandler(Future<Void> resultFuture) {
        return asyncResult -> {
            if (asyncResult.failed()) {
                resultFuture.fail(asyncResult.cause());
                return;
            }
            resultFuture.complete();
        };
    }
}
