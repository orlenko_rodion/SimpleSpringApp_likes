package com.tracks.likes.service;

import com.tracks.likes.model.Track;
import com.tracks.likes.repository.TrackRepository;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;


@Service
public class TrackService {
    private TrackRepository trackRepository;

    private Vertx vertx;

    @Autowired
    public TrackService(TrackRepository trackRepository, Vertx vertx) {
        this.trackRepository = trackRepository;
        this.vertx = vertx;
    }

    public Future<Track> saveAndValidate(Track track) {
        Future<Track> resultFuture = Future.future();

        vertx.executeBlocking(blockingFuture -> {
            validateTrack(track);
            blockingFuture.complete(trackRepository.save(track));
        }, trackAsyncResultHandler(resultFuture));

        return resultFuture;
    }

    public Future<Void> removeTrack(Track track) {
        Future<Void> resultFuture = Future.future();

        vertx.executeBlocking(blockingFuture -> {
            trackRepository.delete(track);
            blockingFuture.complete();
        }, voidAsyncResultHandler(resultFuture));

        return resultFuture;
    }

    public Future<Track> getTrackById(String trackId) {
        Future<Track> resultFuture = Future.future();

        vertx.executeBlocking(blockingFuture -> {
            Optional<Track> trackOptional = trackRepository.findById(trackId);
            if(!trackOptional.isPresent()) {
                blockingFuture.fail("Track with such id does not exists");
                return;
            }
            blockingFuture.complete(trackOptional.get());
        }, trackAsyncResultHandler(resultFuture));

        return resultFuture;
    }

    private void validateTrack(Track track) {
        Objects.requireNonNull(track, "Track should not be null");
        Objects.requireNonNull(track.getTitle(), "The title cannot be null");
    }

    private Handler<AsyncResult<Track>> trackAsyncResultHandler(Future<Track> resultFuture) {
        return asyncResult -> {
            if (asyncResult.failed()) {
                resultFuture.fail(asyncResult.cause());
                return;
            }
            resultFuture.complete(asyncResult.result());
        };
    }

    private Handler<AsyncResult<Void>> voidAsyncResultHandler(Future<Void> resultFuture) {
        return asyncResult -> {
            if (asyncResult.failed()) {
                resultFuture.fail(asyncResult.cause());
                return;
            }
            resultFuture.complete(asyncResult.result());
        };
    }
}
